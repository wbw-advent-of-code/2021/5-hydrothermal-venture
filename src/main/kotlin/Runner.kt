fun runPart1() {
    println(countIntersections(simpleGrid("src/main/resources/input.txt")))
}

fun runPart2() {
    println(countIntersections(fullGrid("src/main/resources/input.txt")))
}

fun main() {
    runPart2()
}