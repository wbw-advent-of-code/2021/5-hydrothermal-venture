import java.io.File
import kotlin.math.max
import kotlin.math.min

data class Coordinate(val x: Int, val y: Int)

enum class Direction {
    HORIZONTAL, VERTICAL, DIAGONAL
}

data class Line(val start: Coordinate, val end: Coordinate) {
    fun direction(): Direction {
        return if (start.y == end.y) Direction.HORIZONTAL
        else if (start.x == end.x) Direction.VERTICAL
        else Direction.DIAGONAL
    }

    fun fullCoordinates(): List<Coordinate> {
        return when (direction()) {
            Direction.HORIZONTAL -> horizontalCoordinates()
            Direction.VERTICAL -> verticalCoordinates()
            Direction.DIAGONAL -> diagonalCoordinate()
        }
    }

    private fun horizontalCoordinates(): List<Coordinate> {
        val start = min(this.start.x, this.end.x)
        val end = max(this.start.x, this.end.x)

        return IntRange(start, end)
            .map { Coordinate(it, this.start.y) }
            .toList()
    }

    private fun verticalCoordinates(): List<Coordinate> {
        val start = min(this.start.y, this.end.y)
        val end = max(this.start.y, this.end.y)

        return IntRange(start, end)
            .map { Coordinate(this.start.x, it) }
            .toList()
    }

    private fun diagonalCoordinate(): List<Coordinate> {
        val left = if(start.x < end.x) start else end
        val right = if(start.x > end.x) start else end

        var yValue = left.y

        val increment = left.y < right.y

        return IntRange(left.x, right.x)
            .map { Coordinate(it, if(increment) yValue++ else yValue--) }
            .toList()
    }
}

fun parseInput(fileName: String): List<Line> {
    return File(fileName).useLines { it.toList() }
        .map { mapLine(it) }
}

fun gridSize(lines: List<Line>): Int {
    return lines.map { max(it.start.x, it.end.x) to max(it.start.y, it.end.y) }
        .map { max(it.first, it.second) }
        .maxOf { it } + 1
}

fun drawSimple(grid: Array<Array<Int>>, lines: List<Line>) {
    lines.filter { it.direction() != Direction.DIAGONAL }
        .forEach { draw(grid, it.fullCoordinates()) }
}

fun draw(grid: Array<Array<Int>>, coordinates: List<Coordinate>) {
    coordinates.forEach {
        grid[it.y][it.x] += 1
    }
}

fun simpleGrid(fileName: String): Array<Array<Int>> {
    val lines = parseInput(fileName)
    val size = gridSize(lines)
    val grid = Array(size) { Array(size) { 0 } }

    drawSimple(grid, lines)

    grid.forEach {
        println(it.joinToString(separator = " "))
    }

    return grid
}

fun fullGrid(fileName: String): Array<Array<Int>> {
    val lines = parseInput(fileName)
    val size = gridSize(lines)
    val grid = Array(size) { Array(size) { 0 } }

    lines.forEach { draw(grid, it.fullCoordinates()) }

    return grid
}

internal fun mapLine(input: String): Line {
    val splitted = input.split(" -> ")

    return Line(mapPair(splitted[0]), mapPair(splitted[1]))
}

internal fun mapPair(input: String): Coordinate {
    val split = input.split(",")
    return Coordinate(split[0].toInt(), split[1].toInt())
}

fun countIntersections(grid: Array<Array<Int>>): Int {
    return grid.flatten()
        .count { it >= 2 }
}