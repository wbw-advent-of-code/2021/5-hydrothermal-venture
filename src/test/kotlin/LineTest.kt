import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.IntStream
import java.util.stream.Stream

internal class LineTest {
    @ParameterizedTest
    @MethodSource("horizontal")
    fun horizontalCoordinates(line: Line, coordinates: List<Coordinate>) {
        assertThat(line.fullCoordinates())
            .containsExactlyInAnyOrderElementsOf(coordinates)
    }

    @ParameterizedTest
    @MethodSource("vertical")
    fun verticalCoordinates(line: Line, coordinates: List<Coordinate>) {
        assertThat(line.fullCoordinates())
            .containsExactlyInAnyOrderElementsOf(coordinates)
    }

    @ParameterizedTest
    @MethodSource("diagonal")
    fun diagonalCoordinates(line: Line, coordinates: List<Coordinate>) {
        assertThat(line.fullCoordinates())
            .containsExactlyInAnyOrderElementsOf(coordinates)
    }

    companion object {
        @JvmStatic
        fun horizontal(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    Line(Coordinate(0, 0), Coordinate(5, 0)),
                    listOf(
                        Coordinate(0, 0),
                        Coordinate(1, 0),
                        Coordinate(2, 0),
                        Coordinate(3, 0),
                        Coordinate(4, 0),
                        Coordinate(5, 0)
                    )
                ),
                Arguments.of(
                    Line(Coordinate(0, 5), Coordinate(8, 5)),
                    listOf(
                        Coordinate(0, 5),
                        Coordinate(1, 5),
                        Coordinate(2, 5),
                        Coordinate(3, 5),
                        Coordinate(4, 5),
                        Coordinate(5, 5),
                        Coordinate(6, 5),
                        Coordinate(7, 5),
                        Coordinate(8, 5)
                    )
                )
            )
        }

        @JvmStatic
        fun vertical(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    Line(Coordinate(0, 0), Coordinate(0, 5)),
                    IntRange(0,5).map { Coordinate(0, it) }.toList()
                ),
                Arguments.of(
                    Line(Coordinate(5, 5), Coordinate(5, 8)),
                    IntRange(5,8).map { Coordinate(5, it) }.toList()
                )
            )
        }

        @JvmStatic
        fun diagonal(): Stream<Arguments> {
            return Stream.of(
                Arguments.of(
                    Line(Coordinate(1,1), Coordinate(6,6)),
                    IntRange(1,6).map { Coordinate(it, it) }.toList()
                ),
                Arguments.of(
                    Line(Coordinate(10,10), Coordinate(6,6)),
                    IntRange(6,10).map { Coordinate(it, it) }.toList()
                ),
            )
        }
    }
}