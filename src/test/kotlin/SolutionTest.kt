import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream
import kotlin.test.assertEquals

class SolutionTest {
    @Test
    internal fun `test day 5 part 1`() {
        assertEquals(5, countIntersections(simpleGrid("src/test/resources/input.txt")))
    }

    @Test
    internal fun `test day 5 part 2`() {
        assertEquals(12, countIntersections(fullGrid("src/test/resources/input.txt")))
    }

    @ParameterizedTest
    @MethodSource("coordinateSrc")
    internal fun testMapPair(input: String, pair: Coordinate) {
        assertEquals(pair, mapPair(input))
    }

    @ParameterizedTest
    @MethodSource("lineSrc")
    internal fun testMapLine(input: String, line: Line) {
        assertEquals(line, mapLine(input))
    }

    companion object {
        @JvmStatic
        fun coordinateSrc(): Stream<Arguments> {
            return Stream.of(
                Arguments.of("0,0", Coordinate(0, 0)),
                Arguments.of("0,1", Coordinate(0, 1)),
                Arguments.of("0,2", Coordinate(0, 2)),
                Arguments.of("1,0", Coordinate(1, 0)),
                Arguments.of("1,1", Coordinate(1, 1)),
                Arguments.of("1,2", Coordinate(1, 2))
            )
        }

        @JvmStatic
        fun lineSrc(): Stream<Arguments> {
            return Stream.of(
                Arguments.of("0,0 -> 0,5", Line(Coordinate(0, 0), Coordinate(0, 5))),
                Arguments.of("0,1 -> 1,5", Line(Coordinate(0, 1), Coordinate(1, 5))),
                Arguments.of("0,2 -> 0,0", Line(Coordinate(0, 2), Coordinate(0, 0))),
                Arguments.of("1,0 -> 0,1", Line(Coordinate(1, 0), Coordinate(0, 1))),
                Arguments.of("1,1 -> 1,1", Line(Coordinate(1, 1), Coordinate(1, 1))),
                Arguments.of("1,2 -> 1,8", Line(Coordinate(1, 2), Coordinate(1, 8)))
            )
        }
    }
}